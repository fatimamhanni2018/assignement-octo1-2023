package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDTO;
import ma.octo.assignement.dto.TransactionDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class TransactionServiceImplTest {
    @Autowired
    private TransactionServiceImpl transactionService;


    @Test
    public void shouldSaveDepositwithSuccess(){

        Deposit expectedDeposit=Deposit.builder()
                .rib("021547896540001")
                .nomPrenomEmetteur("fatima zahra mhanni")
                .montant(new BigDecimal(2543))
                .motif("mon motif")
                .dateExecution(new Date())
                .compteBeneficiaire(new Compte())
                .build();
        Deposit savedDeposit= transactionService.saveDeposit(expectedDeposit);
        assertNotNull(savedDeposit);
        assertNotNull(savedDeposit.getId());
        assertEquals(savedDeposit.getRib(),expectedDeposit.getRib());
        assertEquals(savedDeposit.getNomPrenomEmetteur(),expectedDeposit.getNomPrenomEmetteur());

    }


}