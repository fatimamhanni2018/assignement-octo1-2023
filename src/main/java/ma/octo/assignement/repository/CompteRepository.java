package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CompteRepository extends JpaRepository<Compte, Long> {
  public Compte findByNrCompte(String nrCompte);

  public Compte findByRib(String rib);
  public List<Compte> findByUtilisateur(Utilisateur utilisateur);
}
