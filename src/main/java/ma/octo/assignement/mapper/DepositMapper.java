package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDTO;
import ma.octo.assignement.repository.CompteRepository;
import org.mapstruct.*;
@Mapper
public interface DepositMapper {

    @Mapping(target="rib",source = "compteBeneficiaire.rib")
    DepositDTO depositToDepositDTO(Deposit deposit);
    @Mapping(target="compteBeneficiaire.rib",source = "rib")
    Deposit depositDTOToDeposit(DepositDTO depositDto);

}
