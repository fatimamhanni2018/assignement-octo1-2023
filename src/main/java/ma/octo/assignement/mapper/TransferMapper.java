package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDTO;
import ma.octo.assignement.repository.CompteRepository;
import net.bytebuddy.implementation.bytecode.constant.DefaultValue;
import org.hibernate.cfg.annotations.reflection.internal.XMLContext;
import org.mapstruct.*;

@Mapper
public interface TransferMapper {


    @Mapping(target="nrCompteEmetteur",source = "compteEmetteur.nrCompte")
    @Mapping(target="nrCompteBeneficiaire",source = "compteBeneficiaire.nrCompte")
    TransferDTO transferToTransferDTO(Transfer transfer);
    @Mapping(target="compteEmetteur.nrCompte",source ="nrCompteEmetteur" )
    @Mapping(target="compteBeneficiaire.nrCompte",source ="nrCompteBeneficiaire" )
    Transfer transferDTOToTransfer(TransferDTO transferDto);






}
