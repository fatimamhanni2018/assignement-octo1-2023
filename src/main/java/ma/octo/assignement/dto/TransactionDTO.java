package ma.octo.assignement.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDTO {
    private Long id;
    @NotNull
    private String motif;
    @NotNull
    @Range(min=10,max=10000,message="Montant minimale est 10 et Montant maximal est 10000")
    private BigDecimal montant;
    @NotNull
    private Date date;
}

