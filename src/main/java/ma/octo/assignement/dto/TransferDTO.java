package ma.octo.assignement.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransferDTO extends TransactionDTO {

  @NotNull
  private String nrCompteEmetteur;
  @NotNull
  private String nrCompteBeneficiaire;


}
