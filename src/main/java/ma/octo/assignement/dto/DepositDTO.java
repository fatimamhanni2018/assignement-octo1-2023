package ma.octo.assignement.dto;

import com.sun.istack.NotNull;
import lombok.Data;

@Data

public class DepositDTO extends TransferDTO{

    @NotNull
    private String rib;
    @NotNull
    private String nomPrenomEmetteur;
}
