package ma.octo.assignement.web;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDTO;
import ma.octo.assignement.dto.TransferDTO;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.service.interfaces.AuditTransactionService;
import ma.octo.assignement.service.interfaces.CompteService;
import ma.octo.assignement.service.interfaces.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController(value = "/v1/api/transactions")
class TransactionController {

    Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);
    private final AuditTransactionService auditTransactionService;
    private final TransactionService transactionService;
    private final CompteService compteService;
    private final TransferMapper transferMapper;
    private final DepositMapper depositMapper;


    TransactionController(AuditTransactionService auditTransactionService, TransactionService transactionService, CompteService compteService, TransferMapper transferMapper, DepositMapper depositMapper) {
        this.auditTransactionService = auditTransactionService;
        this.transactionService = transactionService;
        this.compteService = compteService;
        this.transferMapper = transferMapper;
        this.depositMapper = depositMapper;
    }

   /* @GetMapping("listDesTransferts")
    List<Transfer> loadAll() {
        LOGGER.info("Lister des utilisateurs");
        var allTransferts = transferRepository.findAll();

        if (CollectionUtils.isEmpty(allTransferts)) {
            return null;
        } else {
            return CollectionUtils.isEmpty(allTransferts) ? null:allTransferts;
        }
    }*/

 /*   @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {
        List<Compte> allComptes = compteRepository.findAll();

        if (CollectionUtils.isEmpty(allComptes)) {
            return null;
        } else {
            return allComptes;
        }
    }*/

   /* @GetMapping("listerUtilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> allUtilisateurs = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(allUtilisateurs)) {
            return null;
        } else {
            return allUtilisateurs;
        }
    }*/

    @PostMapping("/createTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public TransferDTO createTransfer(@Valid @RequestBody TransferDTO transferDto) //@Valid verifier que les champs sont valides
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
                Transfer transfer = transferMapper.transferDTOToTransfer(transferDto);
                transactionService.createTransfer(transfer);
                transfer = transactionService.saveTransfer(transfer);
                auditTransactionService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString());
                return transferMapper.transferToTransferDTO(transfer);


            }

    @PostMapping("/createDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public DepositDTO createDeposit(@Valid @RequestBody DepositDTO depositDto) //@Valid verifier que les champs sont valides
            throws  CompteNonExistantException, TransactionException {
        Deposit deposit = depositMapper.depositDTOToDeposit(depositDto);
        transactionService.createDeposit(deposit);
        deposit = transactionService.saveDeposit(deposit);
        auditTransactionService.auditDeposit("Un Depos de la part de " + depositDto.getNomPrenomEmetteur() + " au benefice de " + depositDto.getNrCompteBeneficiaire()
                 + " d'un montant de " + depositDto.getMontant()
                .toString());
        return depositMapper.depositToDepositDTO(deposit);
    }

}
