package ma.octo.assignement.service;
import ma.octo.assignement.domain.AuditTransaction;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransactionRepository;
import ma.octo.assignement.service.interfaces.AuditTransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional

public class AuditTransactionServiceImpl implements AuditTransactionService {
    Logger LOGGER = LoggerFactory.getLogger(AuditTransactionServiceImpl.class);
    private final AuditTransactionRepository auditTransactionRepository;

    public AuditTransactionServiceImpl(AuditTransactionRepository auditTransactionRepository) {
        this.auditTransactionRepository = auditTransactionRepository;
    }

    @Override
    public void auditDeposit(String message) {
        LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);

        AuditTransaction audit = new AuditTransaction();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditTransactionRepository.save(audit);
    }

    @Override
    public void auditTransfer(String message) {
        LOGGER.info("Audit de l'événement {}", EventType.TRANSFER);

        AuditTransaction audit = new AuditTransaction();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditTransactionRepository.save(audit);

    }
}
