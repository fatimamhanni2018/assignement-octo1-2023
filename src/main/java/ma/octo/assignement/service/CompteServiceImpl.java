package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.interfaces.CompteService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class CompteServiceImpl implements CompteService {
    private final CompteRepository compteRepository;

    public CompteServiceImpl(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    @Override
    public Compte findByNrCompte(String nrCompte) {
        return compteRepository.findByNrCompte(nrCompte);
    }

    @Override
    public Compte findByRib(String rib) {
        return compteRepository.findByRib(rib);
    }

    @Override
    public List<Compte> findAll() {
        return compteRepository.findAll();
    }

    @Override
    public List<Compte> findByUtilisateur(Utilisateur utilisateur) {
        return compteRepository.findByUtilisateur(utilisateur);
    }
}
