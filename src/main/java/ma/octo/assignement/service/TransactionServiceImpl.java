package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {
    private final DepositRepository depositRepository;
    private final CompteRepository compteRepository;
    private final TransferRepository transferRepository;

    @Autowired
    public TransactionServiceImpl(DepositRepository depositRepository, CompteRepository compteRepository, TransferRepository transferRepository) {
        this.depositRepository = depositRepository;
        this.compteRepository = compteRepository;
        this.transferRepository = transferRepository;
    }


    @Override
    public Deposit saveDeposit(Deposit deposit) {
        return depositRepository.save(deposit);
    }

    @Override
    public List<Deposit> findAllDeposit() {
        return depositRepository.findAll();
    }

    @Override
    public Transfer saveTransfer(Transfer transfer) {
        return transferRepository.save(transfer);
    }

    @Override
    public List<Transfer> findAllTransfer() {
        return transferRepository.findAll();
    }
    @Override
    public void createDeposit(Deposit deposit) {
        Compte compteBeneficiaire =deposit.getCompteBeneficiaire();
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(deposit.getMontant()));
        compteRepository.save(compteBeneficiaire);
    }

    @Override
    public void createTransfer(Transfer transfer) throws SoldeDisponibleInsuffisantException {

        Compte compteEmetteur = transfer.getCompteEmetteur();
        Compte compteBeneficaire = transfer.getCompteBeneficiaire();

        if (compteEmetteur.getSolde().subtract(transfer.getMontant() ).compareTo(BigDecimal.valueOf(0)) ==-1) {
            throw new SoldeDisponibleInsuffisantException("solde insuffisant");
        }
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transfer.getMontant()));
        compteRepository.save(compteEmetteur);

        compteBeneficaire.setSolde(compteBeneficaire.getSolde().add(transfer.getMontant()));
        compteRepository.save(compteBeneficaire);

    }
}
