package ma.octo.assignement.service.interfaces;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;

import java.util.List;

public interface TransactionService {
    public Deposit saveDeposit(Deposit deposit);
    public List<Deposit> findAllDeposit();
    public void createDeposit(Deposit deposit);
    public Transfer saveTransfer(Transfer transfer);
    public List<Transfer> findAllTransfer();
    public void createTransfer(Transfer transfer) throws SoldeDisponibleInsuffisantException;

}
