package ma.octo.assignement.service.interfaces;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;

import java.util.List;

public interface CompteService {
    Compte findByNrCompte(String nrCompte);
    Compte findByRib(String rib);
    List<Compte> findAll();
    List<Compte> findByUtilisateur(Utilisateur utilisatuer);

}
