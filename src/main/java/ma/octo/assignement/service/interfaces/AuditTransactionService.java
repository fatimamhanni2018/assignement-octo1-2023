package ma.octo.assignement.service.interfaces;

public interface AuditTransactionService {
    public void auditDeposit(String message);
    public void auditTransfer(String message);

}
