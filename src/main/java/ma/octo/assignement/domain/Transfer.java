package ma.octo.assignement.domain;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "Transfer")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Transfer extends Transaction {

  @ManyToOne
  private Compte compteEmetteur;

}
