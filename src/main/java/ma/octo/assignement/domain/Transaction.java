package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "Deposit")
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(precision = 16, scale = 2, nullable = false)
    protected BigDecimal montant;

    @Column
    protected Date dateExecution;

    @ManyToOne()
    protected Compte compteBeneficiaire;


    @Column(length = 200)
    protected String motif;
}

