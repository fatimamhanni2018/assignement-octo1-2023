package ma.octo.assignement.domain;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "Deposit")
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Deposit extends Transaction {

  @Column
  private String nomPrenomEmetteur;
  @Column
  private String rib;
}
